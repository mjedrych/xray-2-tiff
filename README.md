# Xray-2-Tiff

Simple tool that converts compressed X-ray images (closed binary .XXr format) from a given directory into tiff files. The most difficult part was to find out how pixel values are stored in the binary files. 