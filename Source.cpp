#include <iostream>
#include <fstream>
#include <vector>
#include <string>

#include <boost/filesystem.hpp>
#include <boost/range/iterator_range.hpp>

#include <windows.h>

#define cimg_use_tiff
#include "CImg.h"


using namespace std;
using namespace cimg_library;

struct bit_field
{
	unsigned x : 12; // 10 bits
};

void windows_system(LPSTR cmd)
{

	STARTUPINFO si;
	PROCESS_INFORMATION pi;
	si.dwFlags = STARTF_USESHOWWINDOW;
	si.wShowWindow = SW_HIDE;

	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));


	if (CreateProcess(NULL,   // No module name (use command line)
		cmd,        // Command line
		NULL,           // Process handle not inheritable
		NULL,           // Thread handle not inheritable
		FALSE,          // Set handle inheritance to FALSE
		0,              // No creation flags
		NULL,           // Use parent's environment block
		NULL,           // Use parent's starting directory 
		&si,            // Pointer to STARTUPINFO structure
		&pi)           // Pointer to PROCESS_INFORMATION structure
		){
		// Wait until child process exits.
		WaitForSingleObject(pi.hProcess, INFINITE);

		// Close process and thread handles. 
		CloseHandle(pi.hProcess);
		CloseHandle(pi.hThread);
	}
}

int main(int argc, char* argv[]) {

//	if (argc > 1) {
		system("mkdir _Output_");
		windows_system("mkdir _Temp_");

		unsigned char* buffer = new unsigned char[2];
		unsigned char* tmp = buffer;

		unsigned short Val;
		buffer = reinterpret_cast<unsigned char*>(&Val);

		for (auto& entry : boost::make_iterator_range(boost::filesystem::directory_iterator("."), {})) {
			string name = entry.path().string();
			name = name.substr(2, name.length());
			
			string ext = "";
			if(name.length()>=4)
				ext = name.substr(name.length() - 4, string::npos);
			name = name.substr(0, name.length() - 4);
			
			if ((ext.length()==4)&&(ext[3] == 'r')&&(ext[1] == '1')) {
				//cout << "--------------------" << endl;
				//cout <<ext << endl;
				//cout << "--------------------" << endl;
				

				string command = "7z\\7za.exe rn " + name + ext + " " + name + " temp";
				std::system(command.c_str());
				command = "7z\\7za.exe e -o_Temp_ -y -bd -bb3  " + name + ext;
				std::system(command.c_str());
				command = "7z\\7za.exe rn " + name + ext + " temp " + name;
				std::system(command.c_str());
				
				string filename = "_Temp_/temp";
				ifstream f(filename.c_str(), ios::binary);
				f.seekg(0, ios::end);
				const int fileLength = static_cast<int>(f.tellg());
				f.seekg(0, ios::beg);

				
				unsigned char* bufferImageData;
				bufferImageData = new unsigned char[fileLength];
				
			
				f.read((char*)bufferImageData, fileLength);
				f.close();
				f.clear();

				string out_file_name = "";

				for (int str_idx = 64 + 2 * 3052 * 2540; str_idx < fileLength; ++str_idx) {
					out_file_name += bufferImageData[str_idx];
				}
				unsigned int id_idx = out_file_name.find("Study-ID=");
				unsigned int rmax_idx = out_file_name.find("R-Max0=");
				string rmax_s = "";
				rmax_s = out_file_name.substr(rmax_idx + 7, 6);
				rmax_s = rmax_s.substr(0, rmax_s.find("\n"));

				double rmax = atof(rmax_s.c_str());

				string study_id = "";
				study_id = out_file_name.substr(id_idx + 9, 10);

				study_id = study_id.substr(0, study_id.find("\n"));

				out_file_name = out_file_name.substr(out_file_name.find("T-VName=") + 8, 20);
				out_file_name = out_file_name.substr(0, out_file_name.find("\n"));


				unsigned int x = 0;
				unsigned int y = 0;
				CImg<unsigned short> img(3052, 2540, 1);
				
				
				for (int counter = 64; counter < 64 + 2 * 3052 * 2540; counter += 2) {
					buffer[0] = bufferImageData[counter];
					buffer[1] = bufferImageData[counter + 1];
					img.atXY(x, y) = Val;
					x++;
					if (x == 3052) {
						x = 0;
						y++;
					}
				}

				img.crop(0, 16, 3051, 2523);
				
				CImg<double> img_negative(img);
				
				double norm = 1.0;
				if (rmax < 4096.0)
					norm = 65535.0 / 4096.0;

				cout << "||||||||||||||||||||||||||||\n";
				cout << norm << "\n";
				cout << rmax << "\n";
				cout << "||||||||||||||||||||||||||||\n";
				//	img_negative = (65535.0*img_negative) / 4095.0;
				
				//img_negative = (-1.0* img_negative) + 65535.0;
				
				for (int i = 0; i < 3052; ++i)
					for (int j = 0; j < 2508; ++j) {
						img.atXY(i, j) = static_cast<unsigned short>(round(-1.0*norm*img.atXY(i, j)+ 65535.0));
					}
						//img.atXY(x, y) = round(-1.0*norm*img.atXY(x, y) + 65535.0);
				
				//img = img_negative;
				//CImg<unsigned char> img8bit(img_negative);
				string temp = out_file_name + "_" + study_id + "_" + name + ".tiff";

				while(temp.find('/') != std::string::npos)
					temp.replace(temp.find('/'), 1 , "-");

				temp = "_Output_/" + temp;

				cout << "-----------------------------" << endl;
				cout << temp << endl;
				cout << "-----------------------------" << endl;
				img.save_tiff(temp.c_str());
			
				
				delete[] bufferImageData;
			}
			
		}
		delete[] tmp;
		
		
		std::system("pause");
	
	return 0;
}


